import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthenticationService} from "../user/authentication.service";
import {Router} from "@angular/router";
import {map} from "rxjs/operators";

interface PhoneCode {
  value: string;
  viewValue: string;
}


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  // @ts-ignore
  registerForm: FormGroup;

  codes: PhoneCode[] = [
    {value: '359', viewValue: '359'},
  ];

  constructor(private authService: AuthenticationService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.registerForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required, Validators.minLength(8)]),
      phoneCode: new FormControl(null, [Validators.required]),
      phone: new FormControl(null, [Validators.required])
    })
  }

  onSubmit() {
    if(this.registerForm.invalid) {
      return;
    }
    this.authService.register(this.registerForm.value).pipe(
      map(() => this.router.navigate(['login']))
    ).subscribe();
  }

  numberOnly(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
}
