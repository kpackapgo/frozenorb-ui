import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {map} from "rxjs/operators";
import {UrlSerializer} from "@angular/router";

export interface LoginForm {
  email: string;
  password: string;
};

export interface RegisterForm {
  email: string
  password: string;
  phoneCode: number;
  phone: number;
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private baseUrl = "http://localhost:8080";

  constructor(private http: HttpClient) {
  }

  login(loginForm: LoginForm) {
    return this.http.post(this.baseUrl+'/login',{email: loginForm.email, password: loginForm.password},  {responseType: 'text' as 'text'})
  }

  register(registerForm: RegisterForm) {
    let userPayload = {
      email: registerForm.email,
      password: registerForm.password,
      phone: registerForm.phoneCode+registerForm.phone
    }
    return this.http.post<any>(this.baseUrl+'/register', userPayload).pipe(
      map(user => user)
    )
  }

  isUserVerified(email: String) {
    return this.http.get<boolean>(this.baseUrl+'/isverified/'+email)
  }

  verify(email: String, verificationCode: string) {
    return this.http.post<any>(this.baseUrl+'/verify',{email: email, verificationCode: verificationCode})
  }
}
