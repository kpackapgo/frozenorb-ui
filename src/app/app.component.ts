import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {AuthenticationService} from "./user/authentication.service";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'FrozenOrb';

  constructor(private router: Router, private authService: AuthenticationService) {}

  navigateTo(value: string) {
    this.router.navigate(['../', value]);
  }

  logout() {
    // this.authService.logout();
  }
}
