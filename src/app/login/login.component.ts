import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from "../user/authentication.service";
import {Router} from "@angular/router";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  // @ts-ignore
  loginForm: FormGroup;

  // @ts-ignore
  verificationForm: FormGroup;

  // @ts-ignore
  errorMessage: string;

  // @ts-ignore
  requireVerification: boolean;

  constructor(private authService: AuthenticationService, private formBuilder: FormBuilder, private router: Router, public dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required, Validators.minLength(8)])
    })

    this.verificationForm = new FormGroup({
      verificationCode: new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(6)]),
    })
  }

  onSubmit() {
    if (this.loginForm.invalid) {
      return;
    }

    this.authService.isUserVerified(this.loginForm.value.email).subscribe(
      data => {
        if (data) {
          console.log("VERIFIED")
          this.authService.login(this.loginForm.value).subscribe(
            () => alert('YOU ARE LOGGED IN. WELCOME!'))
        } else {
          this.requireVerification = true;
          console.log("NOT VERIFIED");
        }
      },
      err => alert(err.error)
    )

    console.log("END OF LOGIN")
  }

  onVerifySubmit() {
    this.authService.verify(this.loginForm.value.email, this.verificationForm.value.verificationCode).subscribe(
      () => {(
        this.dialog.open(VerifiedPopup), this.requireVerification= false)
      },

      err => {(console.log(err.error), alert(err.error))})
  }
}

@Component({
  selector: 'verified-popup',
  templateUrl: 'verified-popup.html',
})
export class VerifiedPopup {}
